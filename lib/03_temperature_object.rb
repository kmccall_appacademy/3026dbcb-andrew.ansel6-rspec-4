class Temperature

  def initialize(options = {})

    defaults = {
      f: nil,
      c: nil
    }

    @options = defaults.merge(options)
  end

  def in_celsius
    return (@options[:f]-32)*(5.0/9.0) if @options[:f] != nil
    @options[:c]
  end

  def in_fahrenheit
    return (@options[:c]*(9.0/5.0))+32 if @options[:c] != nil
    @options[:f]
  end

  def self.from_fahrenheit(temp)
    options = {f: temp}
    Temperature.new(options)
  end

  def self.from_celsius(temp)
    options = {c: temp}
    Temperature.new(options)
  end
end

class Fahrenheit < Temperature
  def initialize(temperature)
    @fahrenheit = temperature
  end

  def in_celsius
    (@fahrenheit-32)*(5.0/9.0)
  end

  def in_fahrenheit
      @fahrenheit
  end
end

class Celsius < Temperature
  def initialize(temperature)
    @celsius = temperature
  end

  def in_celsius
    @celsius
  end

  def in_fahrenheit
      @celsius*(9.0/5.0)+32
  end
end

# class TempFactory
#   TYPES = {
#     celsius: Celsius
#     fahrenheit: Fahrenheit
#   }
#
#   def self.for(type)
#     TYPES[type].
#   end
# end
