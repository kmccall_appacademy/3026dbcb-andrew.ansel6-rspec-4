class Dictionary

  attr_accessor :dictionary
  def initalize
    @hash = {}
  end

  def entries
    return {} if @hash == nil
    @hash
  end

  def add(argument)
    @hash = Hash.new(nil) if @hash.class == NilClass #I don't understand why I need this
    @hash[argument] = nil if argument.class == String
    @hash = @hash.merge(argument) if argument.class == Hash
  end

  def keywords
    @hash.keys.sort
  end

  def include?(string)
    return nil if @hash == nil
    keywords.any? { |key| key ==string }
  end

  def find(string)
    pairs = {}
    return pairs if @hash == nil
    keywords.each { |word| pairs[word] = @hash[word] if word.include?(string) }
    pairs
  end

  def printable
    keywords.each do |key|
      puts "[#{key}] #{@hash[key]}"
    end
  end

  def printable
    printable_strings = []
        keywords.each do |word|
            printable_strings << "[#{word}] \"#{@hash[word]}\"" 
        end
    printable_strings.join("\n")
end
end
