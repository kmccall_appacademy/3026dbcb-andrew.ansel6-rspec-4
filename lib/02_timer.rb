class Timer

  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hours = @seconds / 3600
    @seconds -= 3600*hours

    hours = hours.to_s
    hours.prepend("0") if hours.to_i < 10

    minutes = @seconds / 60
    @seconds -= 60*minutes

    minutes = minutes.to_s
    minutes.prepend("0") if minutes.to_i < 10

    seconds = @seconds.to_s
    seconds.prepend("0") if @seconds.to_i < 10

    "#{hours}:#{minutes}:#{seconds}"
  end
end
