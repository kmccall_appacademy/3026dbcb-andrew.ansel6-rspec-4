class Book

  attr_accessor :title

  def initalize(title)
    @title = capitalize(title)
  end

  def title
    capitalize(@title)
  end

  def capitalize(title)
    lowercase = ["the","a","an","for","and","nor","but","or","yet","so","in","of"]
    arr = title.split(" ")
    arr.each_with_index do |e,i|
      arr[i] = arr[i].capitalize if !lowercase.include?(arr[i]) || i == 0
    end
    arr.join(" ")
  end

end
